﻿using System;
using System.Linq;
using BepInEx;
using HarmonyLib;
using UnityEngine;

namespace UndeadBits.ValheimGravestoneRecovery
{
    [BepInPlugin("UndeadBits.ValheimGravestoneRecovery", "Gravestone Recovery Mod", "1.0.0")]
    public class GravestoneRecoveryPlugin : BaseUnityPlugin
    {
        private readonly Harmony harmony = new Harmony("UndeadBits.ValheimGravestoneRecovery");

        void Awake()
        {
            harmony.PatchAll();
        }

        [HarmonyPatch(typeof(Terminal), "InitTerminal")]
        class RecoverInventory_Patch
        {
            private const float MAX_DISTANCE = 30.0f;

            static void RecoverInventoryOfCurrentPlayer()
            {
                var localPlayer = Player.m_localPlayer;
                if (!localPlayer)
                {
                    Debug.Log("No local player available");
                    return;
                }
                
                var playerProfile = Game.instance.GetPlayerProfile();
                var playerName = playerProfile.GetName();
                
                var allTombStones = FindObjectsOfType<TombStone>(true);
                Debug.Log($"Available tombstones: {String.Join(", ", allTombStones.Select(x => x.GetComponent<Container>().m_name))}");
                
                var allTombStonesForMe = allTombStones
                    .Where(x => x.GetComponent<Container>().m_name == playerName).ToArray();
                
                Debug.Log($"Available tombstones for player {playerName}: {allTombStonesForMe.Length}");
                
                var tombStonesInRange = allTombStonesForMe
                    .Where(x => Vector3.Distance(x.transform.position, localPlayer.transform.position) <= MAX_DISTANCE)
                    .ToArray();
                
                Debug.Log($"Tombstones in range: {String.Join(", ", tombStonesInRange.Select(x => x.GetComponent<Container>().m_name))}");

                var tombStone = tombStonesInRange.FirstOrDefault();
                if (tombStone)
                {
                    Debug.Log($"Opening inventory of tombstone: {tombStone.GetComponent<Container>().m_name}");
                    tombStone.Interact(localPlayer, false, false);
                } else
                {
                    Debug.Log("No tombstone in range");
                }
            }
            
            static void Postfix()
            {
                Debug.Log($"Adding gravestone recovery command to terminal");
                
                var recoverGravestoneCommand = new Terminal.ConsoleCommand("recover_grave", "recovers nearby gravestones", args =>
                {
                    RecoverInventoryOfCurrentPlayer();
                }, false, onlyServer: false, isNetwork: false);
            }
        }
    }
}