# Gravestone Recovery Mod for Valheim

This mod enables players to recover their gravestone, for example, when fallen through the ground, unreachable due to terrain or simply to many monsters lurking around their grave.

## Installation

- Open steam
- Right click on "Valheim"
- Choose `[Manage] -> [Browse local files]`
- Install [BepInEx](https://valheim.thunderstore.io/package/denikson/BepInExPack_Valheim/)
- Move `UndeadBits.ValheimGravestoneRecovery.dll` to `BepInEx/plugins`

## Usage

- Walk near your gravestone
- Press `[Enter]`
- Type `/recover_grave`

## Contribution

To build this mod from source, you have to copy the following libraries from your Valheim game folder:

`BepInEx\core\0Harmony.dll` -> `./lib/BepInEx\core\0Harmony.dll`
`BepInEx\core\BepInEx.dll` -> `./lib/BepInEx\core\BepInEx.dll`
`valheim_Data\Managed\assembly_valheim.dll` -> `./lib/valheim_Data\Managed\assembly_valheim.dll`
`valheim_Data\Managed\UnityEngine.dll` -> `./lib/valheim_Data\Managed\UnityEngine.dll`
`valheim_Data\Managed\UnityEngine.CoreModule.dll` -> `./lib/valheim_Data\Managed\UnityEngine.CoreModule.dll`
